package com.unikent.mjb228.biometricdatacollection.voice.standing;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.unikent.mjb228.biometricdatacollection.R;
import com.unikent.mjb228.biometricdatacollection.voice.VoiceActivity;
import com.unikent.mjb228.biometricdatacollection.voice.treadmill.VoiceTreadmillActivity;

import static com.unikent.mjb228.biometricdatacollection.login.LoginActivity.PARTICIPANT_IDENTIFIER;

public class VoiceStandingActivity extends VoiceActivity {

    private static final String STANDING = "Standing";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_voice_standing);
        createVoiceFile(STANDING);
        super.onCreate(savedInstanceState);
    }

    public void record(View view) {
        recordAction((Button) view, STANDING);
    }

    public void startVoiceTreadmill(View view) {
        Intent voiceTreadmillIntent = new Intent(this, VoiceTreadmillActivity.class);
        voiceTreadmillIntent.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        startActivity(voiceTreadmillIntent);
        finish();
    }

}
