package com.unikent.mjb228.biometricdatacollection.face.standing;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.unikent.mjb228.biometricdatacollection.R;
import com.unikent.mjb228.biometricdatacollection.face.FaceActivity;
import com.unikent.mjb228.biometricdatacollection.face.treadmill.FaceTreadmillActivity;

import static com.unikent.mjb228.biometricdatacollection.login.LoginActivity.PARTICIPANT_IDENTIFIER;
import static com.unikent.mjb228.biometricdatacollection.utilities.device.DeviceHelper.isFaceBiometricAvailable;

public class FaceStandingActivity extends FaceActivity {

    private static final String STANDING = "Standing";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_face_standing);
        if (isFaceBiometricAvailable()) {
            setupHandler();
            setupBufferedWriter(STANDING);
        }
        super.onCreate(savedInstanceState);
    }

    public void startCamera(View view) {
        startCameraActivity(STANDING);
    }

    public void startFaceTreadmill(View view) {
        Intent faceTreadmillIntent = new Intent(this, FaceTreadmillActivity.class);
        faceTreadmillIntent.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        startActivity(faceTreadmillIntent);
        finish();
    }

}
