package com.unikent.mjb228.biometricdatacollection.tour.stop;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.unikent.mjb228.biometricdatacollection.R;
import com.unikent.mjb228.biometricdatacollection.tour.TourStopActivity;
import com.unikent.mjb228.biometricdatacollection.tour.modality.FingerprintTourActivity;

import static com.unikent.mjb228.biometricdatacollection.login.LoginActivity.PARTICIPANT_IDENTIFIER;

public class StopSixActivity extends TourStopActivity {

    private static final String STOP_NUMBER = "6";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stop_six);
    }

    public void arrived(View view) {
        Intent fingerprintTourIntent = new Intent(this, FingerprintTourActivity.class);
        fingerprintTourIntent.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        fingerprintTourIntent.putExtra(TOUR_NUMBER, STOP_NUMBER);
        startActivity(fingerprintTourIntent);
        finish();
    }

}
