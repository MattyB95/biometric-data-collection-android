package com.unikent.mjb228.biometricdatacollection.iris;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.iritech.iddk.android.HIRICAMM;
import com.unikent.mjb228.biometricdatacollection.R;
import com.unikent.mjb228.biometricdatacollection.utilities.iritech.IriShieldHelper;
import com.unikent.mjb228.biometricdatacollection.utilities.iritech.IriShieldUIHelper;

import static com.unikent.mjb228.biometricdatacollection.login.LoginActivity.PARTICIPANT_IDENTIFIER;
import static com.unikent.mjb228.biometricdatacollection.utilities.sensors.SensorService.ATTEMPT;
import static com.unikent.mjb228.biometricdatacollection.utilities.sensors.SensorService.SCENARIO;

public class IriShieldActivity extends AppCompatActivity {

    private HIRICAMM hiricamm;
    private IriShieldHelper iriShieldHelper;
    private Button captureButton;
    private ImageView iriShieldView;
    private IriShieldUIHelper uiHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_iri_shield);
        captureButton = findViewById(R.id.captureButton);
        iriShieldView = findViewById(R.id.iriShieldView);
        Button nextButton = findViewById(R.id.button_next);
        uiHelper = new IriShieldUIHelper(iriShieldView, captureButton, nextButton);
        setupIriShieldHelper();
    }

    @Override
    protected void onPause() {
        super.onPause();
        iriShieldHelper.deinitCamera(hiricamm);
        iriShieldHelper.closeDevice(hiricamm);
    }

    @Override
    public void onBackPressed() {
    }

    private void setupIriShieldHelper() {
        iriShieldView.setBackgroundColor(Color.MAGENTA);
        String participantIdentifier = getIntent().getStringExtra(PARTICIPANT_IDENTIFIER);
        String scenario = getIntent().getStringExtra(SCENARIO);
        String attempt = getIntent().getStringExtra(ATTEMPT);
        iriShieldHelper = new IriShieldHelper(this, participantIdentifier, scenario, attempt);
    }

    public void connect(View view) {
        hiricamm = iriShieldHelper.openDevice(uiHelper);
        if (hiricamm.getHandle() != -1) {
            iriShieldView.setBackgroundColor(Color.GREEN);
            captureButton.setEnabled(true);
        } else {
            iriShieldView.setBackgroundColor(Color.RED);
        }
    }

    public void startCapture(View view) {
        captureButton.setEnabled(false);
        iriShieldHelper.initCamera(hiricamm);
        iriShieldHelper.startCapture(hiricamm);
    }

    public void stopCapture(View view) {
        try {
            iriShieldHelper.stopCapture(hiricamm);
            iriShieldHelper.deinitCamera(hiricamm);
            iriShieldHelper.closeDevice(hiricamm);
        } catch (Exception ignored) {

        }
        finishActivity();
    }

    private void finishActivity() {
        Intent data = new Intent();
        String scenario = getIntent().getStringExtra(SCENARIO);
        data.putExtra(SCENARIO, scenario);
        finish();
    }

}
