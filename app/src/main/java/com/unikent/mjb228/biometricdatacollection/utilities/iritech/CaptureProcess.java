package com.unikent.mjb228.biometricdatacollection.utilities.iritech;

import android.graphics.Bitmap;

import com.iritech.iddk.android.HIRICAMM;
import com.iritech.iddk.android.IddkCaptureProc;
import com.iritech.iddk.android.IddkCaptureStatus;
import com.iritech.iddk.android.IddkImage;
import com.iritech.iddk.android.IddkInteger;
import com.iritech.iddk.android.IddkIrisQuality;
import com.iritech.iddk.android.IddkResult;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.unikent.mjb228.biometricdatacollection.iris.IrisActivity.IRIS;
import static com.unikent.mjb228.biometricdatacollection.utilities.data.DataStorageHelper.FORWARD_SLASH;
import static com.unikent.mjb228.biometricdatacollection.utilities.data.FilenameHelper.UNDERSCORE;
import static com.unikent.mjb228.biometricdatacollection.utilities.data.FilenameHelper.getTimestampedImageFileName;
import static com.unikent.mjb228.biometricdatacollection.utilities.iritech.IriShieldHelper.getResultImage;
import static com.unikent.mjb228.biometricdatacollection.utilities.iritech.IriShieldHelper.getResultQuality;

public class CaptureProcess implements IddkCaptureProc {

    private static final int ZERO = 0;
    private static final int ONE = 1;
    private static final int TWO = 2;
    private static final int THREE = 3;
    private static final int FOUR = 4;
    private static final int QUALITY = 100;

    private final String participantIdentifier;
    private final HIRICAMM hiricamm;
    private final File storageDirectory;
    private final IriShieldUIHelper uiHelper;

    CaptureProcess(String participantIdentifier, HIRICAMM hiricamm, File storageDirectory, IriShieldUIHelper uiHelper) {
        this.participantIdentifier = participantIdentifier;
        this.hiricamm = hiricamm;
        this.storageDirectory = storageDirectory;
        this.uiHelper = uiHelper;
    }

    @Override
    public void invoke(ArrayList<IddkImage> iddkImages, IddkInteger iddkInteger, IddkCaptureStatus iddkCaptureStatus, IddkResult iddkResult) {
        if (iddkCaptureStatus != null) {
            if (iddkCaptureStatus.getValue() == IddkCaptureStatus.IDDK_COMPLETE) {
                IddkImage resultImage = getResultImage(hiricamm);
                if (resultImage == null) {
                    uiHelper.showToast();
                    uiHelper.enableCaptureButton();
                } else {
                    IddkIrisQuality resultQuality = getResultQuality(hiricamm);
                    publishProgress(Collections.singletonList(resultImage));
                    saveIrisImage(resultImage, resultQuality);
                    uiHelper.enableNextButton();
                }
            }
        }
        if (iddkImages != null) {
            publishProgress(iddkImages);
        }
    }

    private void publishProgress(@NotNull List<IddkImage> iddkImages) {
        for (IddkImage iddkImage : iddkImages) {
            Bitmap bitmap = convertBitmap(iddkImage.getImageData(), iddkImage.getImageWidth(), iddkImage.getImageHeight());
            uiHelper.displayImage(bitmap);
        }
    }

    private void saveIrisImage(@NotNull IddkImage iddkImage, @NotNull IddkIrisQuality resultQuality) {
        String jpegFileName = participantIdentifier + UNDERSCORE + resultQuality.getTotalScore() + getTimestampedImageFileName(IRIS);
        try {
            saveJpegImage(iddkImage, jpegFileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void saveJpegImage(@NotNull IddkImage iddkImage, String jpegFileName) throws IOException {
        String jpegFilePath = storageDirectory.getAbsolutePath() + FORWARD_SLASH + jpegFileName;
        FileOutputStream jpegFileOutputStream = new FileOutputStream(jpegFilePath);
        Bitmap bitmap = convertBitmap(iddkImage.getImageData(), iddkImage.getImageWidth(), iddkImage.getImageHeight());
        bitmap.compress(Bitmap.CompressFormat.JPEG, QUALITY, jpegFileOutputStream);
        jpegFileOutputStream.flush();
        jpegFileOutputStream.close();
    }

    @NotNull
    private Bitmap convertBitmap(@NotNull byte[] rawImage, int imageWidth, int imageHeight) {
        byte[] Bits = new byte[rawImage.length * FOUR];
        for (int j = ZERO; j < rawImage.length; j++) {
            Bits[j * FOUR] = rawImage[j];
            Bits[j * FOUR + ONE] = rawImage[j];
            Bits[j * FOUR + TWO] = rawImage[j];
            Bits[j * FOUR + THREE] = -ONE;
        }
        Bitmap mCurrentBitmap = Bitmap.createBitmap(imageWidth, imageHeight, Bitmap.Config.ARGB_8888);
        mCurrentBitmap.copyPixelsFromBuffer(ByteBuffer.wrap(Bits));
        return mCurrentBitmap;
    }

}
