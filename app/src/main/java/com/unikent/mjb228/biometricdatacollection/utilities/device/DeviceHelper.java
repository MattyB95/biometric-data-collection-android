package com.unikent.mjb228.biometricdatacollection.utilities.device;

import android.os.Build;

public class DeviceHelper {

    private static final String SAMSUNG = "samsung";
    private static final String GALAXY = "SM-G960U1";

    public static boolean isFingerprintBiometricAvailable() {
        return isSamsungGalaxyS9();
    }

    public static boolean isFaceBiometricAvailable() {
        return isSamsungGalaxyS9();
    }

    public static boolean isIrisBiometricAvailable() {
        return isSamsungGalaxyS9();
    }

    private static boolean isSamsungGalaxyS9() {
        return Build.MANUFACTURER.equals(SAMSUNG) && Build.MODEL.equals(GALAXY);
    }

}
