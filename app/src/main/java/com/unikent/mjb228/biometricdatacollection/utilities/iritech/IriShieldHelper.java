package com.unikent.mjb228.biometricdatacollection.utilities.iritech;

import android.content.Context;

import com.iritech.iddk.android.HIRICAMM;
import com.iritech.iddk.android.Iddk2000Apis;
import com.iritech.iddk.android.IddkCaptureMode;
import com.iritech.iddk.android.IddkCaptureOperationMode;
import com.iritech.iddk.android.IddkEyeSubType;
import com.iritech.iddk.android.IddkImage;
import com.iritech.iddk.android.IddkImageFormat;
import com.iritech.iddk.android.IddkImageKind;
import com.iritech.iddk.android.IddkInteger;
import com.iritech.iddk.android.IddkIrisQuality;
import com.iritech.iddk.android.IddkQualityMode;
import com.iritech.iddk.android.IddkResult;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.ArrayList;

import static com.unikent.mjb228.biometricdatacollection.iris.IrisActivity.IRIS;
import static com.unikent.mjb228.biometricdatacollection.utilities.data.DataStorageHelper.getParticipantScenarioAttemptDir;

public class IriShieldHelper {

    private static final int ZERO = 0;
    private static final IddkCaptureMode CAPTURE_MODE = new IddkCaptureMode(IddkCaptureMode.IDDK_TIMEBASED);
    private static final int CAPTURE_MODE_PARAMETER = 3;
    private static final IddkQualityMode QUALITY_MODE = new IddkQualityMode(IddkQualityMode.IDDK_QUALITY_NORMAL);
    private static final IddkCaptureOperationMode OPERATION_MODE = new IddkCaptureOperationMode(IddkCaptureOperationMode.IDDK_AUTO_CAPTURE);
    private static final IddkEyeSubType EYE_SUB_TYPE = new IddkEyeSubType(IddkEyeSubType.IDDK_UNKNOWN_EYE);
    private static final boolean AUTO_LEDS = true;
    private static final IddkImageKind RESULT_IMAGE_KIND = new IddkImageKind(IddkImageKind.IDDK_IKIND_K1);
    private static final IddkImageFormat RESULT_IMAGE_FORMAT = new IddkImageFormat(IddkImageFormat.IDDK_IFORMAT_MONO_RAW);
    private static final byte COMPRESS_QUALITY = 100;
    private static final IddkInteger MAX_EYE_SUBTYPES = new IddkInteger();

    private static Iddk2000Apis iddk2000Apis;

    private final String participantIdentifier;
    private final File storageDirectory;
    private CaptureProcess captureProcess;

    public IriShieldHelper(Context context, String participantIdentifier, String scenario, String attemptNumber) {
        iddk2000Apis = Iddk2000Apis.getInstance(context);
        this.participantIdentifier = participantIdentifier;
        storageDirectory = getParticipantScenarioAttemptDir(context, participantIdentifier, IRIS, scenario, attemptNumber);
    }

    public HIRICAMM openDevice(IriShieldUIHelper uiHelper) {
        HIRICAMM hiricamm = new HIRICAMM();
        ArrayList<String> deviceDescriptions = scanDevices();
        if (!deviceDescriptions.isEmpty()) {
            iddk2000Apis.openDevice(deviceDescriptions.get(ZERO), hiricamm);
            captureProcess = new CaptureProcess(participantIdentifier, hiricamm, storageDirectory, uiHelper);
        }
        return hiricamm;
    }

    @NotNull
    private ArrayList<String> scanDevices() {
        ArrayList<String> deviceDescriptions = new ArrayList<>();
        iddk2000Apis.scanDevices(deviceDescriptions);
        return deviceDescriptions;
    }

    public void closeDevice(HIRICAMM hiricamm) {
        iddk2000Apis.closeDevice(hiricamm);
    }

    public void initCamera(HIRICAMM hiricamm) {
        IddkInteger imageWidth = new IddkInteger();
        IddkInteger imageHeight = new IddkInteger();
        iddk2000Apis.initCamera(hiricamm, imageWidth, imageHeight);
    }

    public void deinitCamera(HIRICAMM hiricamm) {
        iddk2000Apis.deinitCamera(hiricamm);
    }

    public void startCapture(HIRICAMM hiricamm) {
        iddk2000Apis.startCapture(hiricamm, CAPTURE_MODE, CAPTURE_MODE_PARAMETER, QUALITY_MODE, OPERATION_MODE, EYE_SUB_TYPE, AUTO_LEDS, captureProcess);
    }

    public void stopCapture(HIRICAMM hiricamm) {
        iddk2000Apis.stopCapture(hiricamm);
    }

    @Nullable
    static IddkImage getResultImage(HIRICAMM hiricamm) {
        ArrayList<IddkImage> iddkImages = new ArrayList<>();
        IddkResult resultImage = iddk2000Apis.getResultImage(hiricamm, RESULT_IMAGE_KIND, RESULT_IMAGE_FORMAT, COMPRESS_QUALITY, iddkImages, MAX_EYE_SUBTYPES);
        return resultImage.getValue() == IddkResult.IDDK_OK ? iddkImages.get(ZERO) : null;
    }

    static IddkIrisQuality getResultQuality(HIRICAMM hiricamm) {
        ArrayList<IddkIrisQuality> iddkIrisQualities = new ArrayList<>();
        iddk2000Apis.getResultQuality(hiricamm, iddkIrisQualities, MAX_EYE_SUBTYPES);
        return iddkIrisQualities.get(ZERO);
    }

}
