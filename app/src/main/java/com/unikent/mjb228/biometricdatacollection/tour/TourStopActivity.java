package com.unikent.mjb228.biometricdatacollection.tour;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.unikent.mjb228.biometricdatacollection.tour.stop.StopEightActivity;
import com.unikent.mjb228.biometricdatacollection.tour.stop.StopFiveActivity;
import com.unikent.mjb228.biometricdatacollection.tour.stop.StopFourActivity;
import com.unikent.mjb228.biometricdatacollection.tour.stop.StopNineActivity;
import com.unikent.mjb228.biometricdatacollection.tour.stop.StopSevenActivity;
import com.unikent.mjb228.biometricdatacollection.tour.stop.StopSixActivity;
import com.unikent.mjb228.biometricdatacollection.tour.stop.StopTenActivity;
import com.unikent.mjb228.biometricdatacollection.tour.stop.StopThreeActivity;
import com.unikent.mjb228.biometricdatacollection.tour.stop.StopTwoActivity;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import static com.unikent.mjb228.biometricdatacollection.login.LoginActivity.PARTICIPANT_IDENTIFIER;

public class TourStopActivity extends AppCompatActivity {

    public static final String TOUR_NUMBER = "TOUR_NUMBER";

    protected String participantIdentifier;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        participantIdentifier = getIntent().getStringExtra(PARTICIPANT_IDENTIFIER);
    }

    @NotNull
    @Contract("_, _ -> new")
    static public Intent getNextStopIntent(Context context, String currentStopNumber) {
        int nextStopNumber = Integer.parseInt(currentStopNumber);
        switch (++nextStopNumber) {
            case 2:
                return new Intent(context, StopTwoActivity.class);
            case 3:
                return new Intent(context, StopThreeActivity.class);
            case 4:
                return new Intent(context, StopFourActivity.class);
            case 5:
                return new Intent(context, StopFiveActivity.class);
            case 6:
                return new Intent(context, StopSixActivity.class);
            case 7:
                return new Intent(context, StopSevenActivity.class);
            case 8:
                return new Intent(context, StopEightActivity.class);
            case 9:
                return new Intent(context, StopNineActivity.class);
            case 10:
                return new Intent(context, StopTenActivity.class);
            default:
                return new Intent(context, TourFinishedActivity.class);
        }
    }

}
