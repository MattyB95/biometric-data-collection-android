package com.unikent.mjb228.biometricdatacollection.tour;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.unikent.mjb228.biometricdatacollection.R;
import com.unikent.mjb228.biometricdatacollection.home.SessionTwoHomeActivity;

import static com.unikent.mjb228.biometricdatacollection.login.LoginActivity.PARTICIPANT_IDENTIFIER;

public class TourFinishedActivity extends AppCompatActivity {

    private String participantIdentifier;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tour_finished);
        participantIdentifier = getIntent().getStringExtra(PARTICIPANT_IDENTIFIER);
    }

    public void finished(View view) {
        Intent sessionTwoHomeIntent = new Intent(this, SessionTwoHomeActivity.class);
        sessionTwoHomeIntent.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        startActivity(sessionTwoHomeIntent);
    }

}
