package com.unikent.mjb228.biometricdatacollection.iris.sitting;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;

import com.unikent.mjb228.biometricdatacollection.R;
import com.unikent.mjb228.biometricdatacollection.iris.IrisActivity;
import com.unikent.mjb228.biometricdatacollection.iris.standing.IrisStandingActivity;

import static com.unikent.mjb228.biometricdatacollection.login.LoginActivity.PARTICIPANT_IDENTIFIER;
import static com.unikent.mjb228.biometricdatacollection.utilities.device.DeviceHelper.isIrisBiometricAvailable;

public class IrisSittingActivity extends IrisActivity {

    private static final String SITTING = "Sitting";

    private static final String CHANGE_TO_IRIS = "Change to Iris";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_iris_sitting);
        if (isIrisBiometricAvailable()) {
            ActivityResultLauncher<Intent> launcher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
            });
            launcher.launch(new Intent(Settings.ACTION_SECURITY_SETTINGS));
            Toast.makeText(this, CHANGE_TO_IRIS, Toast.LENGTH_SHORT).show();
            setupHandler();
            setupBufferedWriter(SITTING);
        }
        super.onCreate(savedInstanceState);
    }

    public void startCamera(View view) {
        startCameraActivity(SITTING);
    }

    public void startIrisStanding(View view) {
        Intent irisStandingIntent = new Intent(this, IrisStandingActivity.class);
        irisStandingIntent.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        startActivity(irisStandingIntent);
        finish();
    }

}
