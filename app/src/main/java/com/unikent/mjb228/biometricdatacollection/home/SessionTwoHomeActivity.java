package com.unikent.mjb228.biometricdatacollection.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.unikent.mjb228.biometricdatacollection.R;
import com.unikent.mjb228.biometricdatacollection.tour.stop.StopEightActivity;
import com.unikent.mjb228.biometricdatacollection.tour.stop.StopFiveActivity;
import com.unikent.mjb228.biometricdatacollection.tour.stop.StopFourActivity;
import com.unikent.mjb228.biometricdatacollection.tour.stop.StopNineActivity;
import com.unikent.mjb228.biometricdatacollection.tour.stop.StopOneActivity;
import com.unikent.mjb228.biometricdatacollection.tour.stop.StopSevenActivity;
import com.unikent.mjb228.biometricdatacollection.tour.stop.StopSixActivity;
import com.unikent.mjb228.biometricdatacollection.tour.stop.StopTenActivity;
import com.unikent.mjb228.biometricdatacollection.tour.stop.StopThreeActivity;
import com.unikent.mjb228.biometricdatacollection.tour.stop.StopTwoActivity;

import static com.unikent.mjb228.biometricdatacollection.login.LoginActivity.PARTICIPANT_IDENTIFIER;

public class SessionTwoHomeActivity extends AppCompatActivity {

    private String participantIdentifier;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session_two_home);
        participantIdentifier = getIntent().getStringExtra(PARTICIPANT_IDENTIFIER);
    }

    public void startStopOne(View view) {
        Intent stopOneIntent = new Intent(this, StopOneActivity.class);
        stopOneIntent.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        startActivity(stopOneIntent);
    }

    public void startStopTwo(View view) {
        Intent stopTwoIntent = new Intent(this, StopTwoActivity.class);
        stopTwoIntent.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        startActivity(stopTwoIntent);
    }

    public void startStopThree(View view) {
        Intent stopThreeIntent = new Intent(this, StopThreeActivity.class);
        stopThreeIntent.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        startActivity(stopThreeIntent);
    }

    public void startStopFour(View view) {
        Intent stopFourIntent = new Intent(this, StopFourActivity.class);
        stopFourIntent.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        startActivity(stopFourIntent);
    }

    public void startStopFive(View view) {
        Intent stopFiveIntent = new Intent(this, StopFiveActivity.class);
        stopFiveIntent.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        startActivity(stopFiveIntent);
    }

    public void startStopSix(View view) {
        Intent stopSixIntent = new Intent(this, StopSixActivity.class);
        stopSixIntent.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        startActivity(stopSixIntent);
    }

    public void startStopSeven(View view) {
        Intent stopSevenIntent = new Intent(this, StopSevenActivity.class);
        stopSevenIntent.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        startActivity(stopSevenIntent);
    }

    public void startStopEight(View view) {
        Intent stopEightIntent = new Intent(this, StopEightActivity.class);
        stopEightIntent.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        startActivity(stopEightIntent);
    }

    public void startStopNine(View view) {
        Intent stopNineIntent = new Intent(this, StopNineActivity.class);
        stopNineIntent.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        startActivity(stopNineIntent);
    }

    public void startStopTen(View view) {
        Intent stopTenIntent = new Intent(this, StopTenActivity.class);
        stopTenIntent.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        startActivity(stopTenIntent);
    }

}
