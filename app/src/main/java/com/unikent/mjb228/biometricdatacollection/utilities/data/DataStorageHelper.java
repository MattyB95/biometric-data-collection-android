package com.unikent.mjb228.biometricdatacollection.utilities.data;

import android.content.Context;

import org.jetbrains.annotations.NotNull;

import java.io.File;

import static java.util.Objects.requireNonNull;

public class DataStorageHelper {

    public static final String FORWARD_SLASH = "/";

    @NotNull
    public static File getParticipantStorageDir(@NotNull Context context, String participantIdentifier) {
        File externalFilesDir = context.getExternalFilesDir(null);
        String externalFilesPath = requireNonNull(externalFilesDir).getAbsolutePath();
        String participantDirPath = externalFilesPath + FORWARD_SLASH + participantIdentifier;
        File participantDir = new File(participantDirPath);
        makeDirectories(participantDir);
        return participantDir;
    }

    @NotNull
    public static File getParticipantScenarioAttemptDir(Context context, String participantIdentifier, String activityDirName, String scenario, String attemptNumber) {
        File participantStorageDir = getParticipantStorageDir(context, participantIdentifier);
        String subDirPath = participantStorageDir.getAbsolutePath() + FORWARD_SLASH + activityDirName + FORWARD_SLASH + scenario + FORWARD_SLASH + attemptNumber;
        File subDir = new File(subDirPath);
        makeDirectories(subDir);
        return subDir;
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private static void makeDirectories(@NotNull File directory) {
        directory.mkdirs();
    }

}
