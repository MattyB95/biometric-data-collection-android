package com.unikent.mjb228.biometricdatacollection.fingerprint.corridor;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.unikent.mjb228.biometricdatacollection.R;
import com.unikent.mjb228.biometricdatacollection.fingerprint.FingerprintActivity;
import com.unikent.mjb228.biometricdatacollection.home.SessionOneHomeActivity;

import static com.unikent.mjb228.biometricdatacollection.login.LoginActivity.PARTICIPANT_IDENTIFIER;

public class FingerprintCorridorActivity extends FingerprintActivity {

    private static final String CORRIDOR = "Corridor";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_fingerprint_corridor);
        setupHandler();
        setupBufferedWriter(CORRIDOR);
        super.onCreate(savedInstanceState);
    }

    public void startBiometricAuthentication(View view) {
        useBiometricPrompt(CORRIDOR);
    }

    public void startSessionOneHome(View view) {
        Intent sessionOneHomeIntent = new Intent(this, SessionOneHomeActivity.class);
        sessionOneHomeIntent.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        startActivity(sessionOneHomeIntent);
        finish();
    }

}
