package com.unikent.mjb228.biometricdatacollection.iris;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import com.unikent.mjb228.biometricdatacollection.R;
import com.unikent.mjb228.biometricdatacollection.utilities.biometric.BiometricPromptHelper;
import com.unikent.mjb228.biometricdatacollection.utilities.sensors.SensorService;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Objects;

import static com.unikent.mjb228.biometricdatacollection.login.LoginActivity.PARTICIPANT_IDENTIFIER;
import static com.unikent.mjb228.biometricdatacollection.utilities.data.DataStorageHelper.getParticipantScenarioAttemptDir;
import static com.unikent.mjb228.biometricdatacollection.utilities.device.DeviceHelper.isIrisBiometricAvailable;
import static com.unikent.mjb228.biometricdatacollection.utilities.sensors.SensorService.ATTEMPT;
import static com.unikent.mjb228.biometricdatacollection.utilities.sensors.SensorService.MODALITY;
import static com.unikent.mjb228.biometricdatacollection.utilities.sensors.SensorService.SCENARIO;

public class IrisActivity extends AppCompatActivity {

    public static final String IRIS = "Iris";

    private static final int ONE = 1;
    private static final int MINIMUM_ATTEMPTS = 5;
    private static final String IRIS_ACTIVITY_CSV = "/irisActivity.csv";
    private static final String COMMA = ",";
    private static final String NEWLINE = "\n";

    private ActivityResultLauncher<Intent> launcher;
    private BiometricPromptHelper biometricPromptHelper;
    private BufferedWriter bufferedWriter;
    private Handler handler;
    private Intent sensorService;
    private long startNanoTime;

    protected String participantIdentifier;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        participantIdentifier = getIntent().getStringExtra(PARTICIPANT_IDENTIFIER);
        launcher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), this::handleIrisResult);
        if (isIrisBiometricAvailable()) {
            biometricPromptHelper = new BiometricPromptHelper(IRIS, this, handler);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (bufferedWriter != null) {
            try {
                bufferedWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
    }

    private void handleIrisResult(@NotNull ActivityResult result) {
        String scenario = Objects.requireNonNull(result.getData()).getStringExtra(SCENARIO);
        if (isIrisBiometricAvailable()) {
            useBiometricPrompt(scenario);
        } else {
            stopService(sensorService);
            updateCounter();
        }
    }

    @NotNull
    private String getCurrentAttempt() {
        TextView counterView = findViewById(R.id.text_view_counter);
        String counterString = counterView.getText().toString();
        int counter = Integer.parseInt(counterString);
        int attempt = counter + ONE;
        return String.valueOf(attempt);
    }

    private void updateCounter() {
        TextView counterView = findViewById(R.id.text_view_counter);
        String currentAttempt = getCurrentAttempt();
        counterView.setText(currentAttempt);
        if (Integer.parseInt(currentAttempt) >= MINIMUM_ATTEMPTS) {
            Button nextButton = findViewById(R.id.button_next);
            nextButton.setVisibility(View.VISIBLE);
        }
    }

    private void startSensorService(String scenario) {
        sensorService = new Intent(this, SensorService.class);
        sensorService.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        sensorService.putExtra(MODALITY, IRIS);
        sensorService.putExtra(SCENARIO, scenario);
        sensorService.putExtra(ATTEMPT, getCurrentAttempt());
        startService(sensorService);
    }

    private void useBiometricPrompt(String scenario) {
        setupBufferedWriter(scenario);
        startSensorService(scenario);
        startNanoTime = System.nanoTime();
        biometricPromptHelper.biometricPromptAuthentication();
    }

    protected void setupHandler() {
        handler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(@NotNull Message message) {
                String timeElapsed = Long.toString(System.nanoTime() - startNanoTime);
                String currentTimeMillis = Long.toString(System.currentTimeMillis());
                try {
                    bufferedWriter.append(message.obj.toString())
                            .append(COMMA)
                            .append(timeElapsed)
                            .append(COMMA)
                            .append(currentTimeMillis)
                            .append(NEWLINE);
                    if (biometricPromptHelper.isAuthenticationComplete()) {
                        stopService(sensorService);
                        bufferedWriter.close();
                        updateCounter();
                    } else {
                        startNanoTime = System.nanoTime();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    protected void setupBufferedWriter(String scenario) {
        participantIdentifier = getIntent().getStringExtra(PARTICIPANT_IDENTIFIER);
        File irisStorageDir = getParticipantScenarioAttemptDir(this, participantIdentifier, IRIS, scenario, getCurrentAttempt());
        String filePath = irisStorageDir.getAbsolutePath() + IRIS_ACTIVITY_CSV;
        File file = new File(filePath);
        try {
            bufferedWriter = new BufferedWriter(new FileWriter(file));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void startIriShieldActivity(String scenario) {
        Intent iriShieldIntent = new Intent(this, IriShieldActivity.class);
        iriShieldIntent.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        iriShieldIntent.putExtra(SCENARIO, scenario);
        iriShieldIntent.putExtra(ATTEMPT, getCurrentAttempt());
        if (!isIrisBiometricAvailable()) {
            startSensorService(scenario);
        }
        launcher.launch(iriShieldIntent);
    }

    protected void startCameraActivity(String scenario) {
        startIriShieldActivity(scenario);
    }

}
