package com.unikent.mjb228.biometricdatacollection.face.corridor;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.unikent.mjb228.biometricdatacollection.R;
import com.unikent.mjb228.biometricdatacollection.face.FaceActivity;
import com.unikent.mjb228.biometricdatacollection.home.SessionOneHomeActivity;

import static com.unikent.mjb228.biometricdatacollection.login.LoginActivity.PARTICIPANT_IDENTIFIER;
import static com.unikent.mjb228.biometricdatacollection.utilities.device.DeviceHelper.isFaceBiometricAvailable;

public class FaceCorridorActivity extends FaceActivity {

    private static final String CORRIDOR = "Corridor";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_face_corridor);
        if (isFaceBiometricAvailable()) {
            setupHandler();
            setupBufferedWriter(CORRIDOR);
        }
        super.onCreate(savedInstanceState);
    }

    public void startCamera(View view) {
        startCameraActivity(CORRIDOR);
    }

    public void startSessionOneHome(View view) {
        Intent sessionOneHomeIntent = new Intent(this, SessionOneHomeActivity.class);
        sessionOneHomeIntent.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        startActivity(sessionOneHomeIntent);
        finish();
    }

}
