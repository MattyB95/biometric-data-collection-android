package com.unikent.mjb228.biometricdatacollection.utilities.data;

import org.jetbrains.annotations.NotNull;

public class FilenameHelper {

    public static final String UNDERSCORE = "_";

    private static final String IMAGE_SUFFIX = ".jpg";
    private static final String AUDIO_SUFFIX = ".3gp";

    @NotNull
    public static String getTimestampedImageFileName(String modality) {
        return UNDERSCORE + modality + UNDERSCORE + System.currentTimeMillis() + IMAGE_SUFFIX;
    }

    @NotNull
    public static String getTimestampedAudioFileName(String modality) {
        return UNDERSCORE + modality + UNDERSCORE + System.currentTimeMillis() + AUDIO_SUFFIX;
    }

}
