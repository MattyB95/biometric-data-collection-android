package com.unikent.mjb228.biometricdatacollection.utilities.sensors;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

class SensorHandler {

    private static final int SAMPLING_PERIOD = 50000;

    private final SensorManager sensorManager;
    private final List<Sensor> sensors = new ArrayList<>();

    SensorHandler(@NotNull Context context) {
        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
    }

    void activateDefaultSensor(int sensorType) {
        Sensor defaultSensor = sensorManager.getDefaultSensor(sensorType);
        if (defaultSensor != null) {
            sensors.add(defaultSensor);
        }
    }

    void registerListener(SensorEventListener listener) {
        for (Sensor sensor : sensors) {
            sensorManager.registerListener(listener, sensor, SAMPLING_PERIOD);
        }
    }

    void unregisterListener(SensorEventListener listener) {
        sensorManager.unregisterListener(listener);
    }

}
