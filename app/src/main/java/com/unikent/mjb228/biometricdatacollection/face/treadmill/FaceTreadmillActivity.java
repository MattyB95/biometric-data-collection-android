package com.unikent.mjb228.biometricdatacollection.face.treadmill;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.unikent.mjb228.biometricdatacollection.R;
import com.unikent.mjb228.biometricdatacollection.face.FaceActivity;
import com.unikent.mjb228.biometricdatacollection.face.corridor.FaceCorridorActivity;

import static com.unikent.mjb228.biometricdatacollection.login.LoginActivity.PARTICIPANT_IDENTIFIER;
import static com.unikent.mjb228.biometricdatacollection.utilities.device.DeviceHelper.isFaceBiometricAvailable;

public class FaceTreadmillActivity extends FaceActivity {

    private static final String TREADMILL = "Treadmill";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_face_treadmill);
        if (isFaceBiometricAvailable()) {
            setupHandler();
            setupBufferedWriter(TREADMILL);
        }
        super.onCreate(savedInstanceState);
    }

    public void startCamera(View view) {
        startCameraActivity(TREADMILL);
    }

    public void startFaceCorridor(View view) {
        Intent faceCorridorIntent = new Intent(this, FaceCorridorActivity.class);
        faceCorridorIntent.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        startActivity(faceCorridorIntent);
        finish();
    }

}
