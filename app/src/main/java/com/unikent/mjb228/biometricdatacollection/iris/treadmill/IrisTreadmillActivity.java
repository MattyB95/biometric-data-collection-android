package com.unikent.mjb228.biometricdatacollection.iris.treadmill;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.unikent.mjb228.biometricdatacollection.R;
import com.unikent.mjb228.biometricdatacollection.iris.IrisActivity;
import com.unikent.mjb228.biometricdatacollection.iris.corridor.IrisCorridorActivity;

import static com.unikent.mjb228.biometricdatacollection.login.LoginActivity.PARTICIPANT_IDENTIFIER;
import static com.unikent.mjb228.biometricdatacollection.utilities.device.DeviceHelper.isIrisBiometricAvailable;

public class IrisTreadmillActivity extends IrisActivity {

    private static final String TREADMILL = "Treadmill";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_iris_treadmill);
        if (isIrisBiometricAvailable()) {
            setupHandler();
            setupBufferedWriter(TREADMILL);
        }
        super.onCreate(savedInstanceState);
    }

    public void startCamera(View view) {
        startCameraActivity(TREADMILL);
    }

    public void startIrisCorridor(View view) {
        Intent irisCorridorIntent = new Intent(this, IrisCorridorActivity.class);
        irisCorridorIntent.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        startActivity(irisCorridorIntent);
        finish();
    }

}
