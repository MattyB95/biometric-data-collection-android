package com.unikent.mjb228.biometricdatacollection.voice.factor;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.unikent.mjb228.biometricdatacollection.R;
import com.unikent.mjb228.biometricdatacollection.fingerprint.factor.FingerprintFactorWetActivity;
import com.unikent.mjb228.biometricdatacollection.voice.VoiceActivity;

import static com.unikent.mjb228.biometricdatacollection.login.LoginActivity.PARTICIPANT_IDENTIFIER;

public class VoiceFactorLoudActivity extends VoiceActivity {

    private static final String FACTOR = "Factor - Loud";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_voice_factor_loud);
        createVoiceFile(FACTOR);
        super.onCreate(savedInstanceState);
    }

    public void record(View view) {
        recordAction((Button) view, FACTOR);
    }

    public void startFingerprintFactorWet(View view) {
        Intent fingerprintFactorWetIntent = new Intent(this, FingerprintFactorWetActivity.class);
        fingerprintFactorWetIntent.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        startActivity(fingerprintFactorWetIntent);
        finish();
    }

}
