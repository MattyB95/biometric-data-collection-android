package com.unikent.mjb228.biometricdatacollection.utilities.camera;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.unikent.mjb228.biometricdatacollection.R;

import static com.unikent.mjb228.biometricdatacollection.login.LoginActivity.PARTICIPANT_IDENTIFIER;
import static com.unikent.mjb228.biometricdatacollection.utilities.sensors.SensorService.ATTEMPT;
import static com.unikent.mjb228.biometricdatacollection.utilities.sensors.SensorService.MODALITY;
import static com.unikent.mjb228.biometricdatacollection.utilities.sensors.SensorService.SCENARIO;

public class CameraActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        if (null == savedInstanceState) {
            String participantIdentifier = getIntent().getStringExtra(PARTICIPANT_IDENTIFIER);
            String modality = getIntent().getStringExtra(MODALITY);
            String scenario = getIntent().getStringExtra(SCENARIO);
            String attempt = getIntent().getStringExtra(ATTEMPT);
            Bundle bundle = new Bundle();
            bundle.putString(PARTICIPANT_IDENTIFIER, participantIdentifier);
            bundle.putString(MODALITY, modality);
            bundle.putString(SCENARIO, scenario);
            bundle.putString(ATTEMPT, attempt);
            CameraFragment cameraFragment = new CameraFragment();
            cameraFragment.setArguments(bundle);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, cameraFragment)
                    .commit();
        }
    }

    @Override
    public void onBackPressed() {
    }

}
