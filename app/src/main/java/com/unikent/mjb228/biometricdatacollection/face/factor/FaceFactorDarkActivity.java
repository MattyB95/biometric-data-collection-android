package com.unikent.mjb228.biometricdatacollection.face.factor;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;

import com.unikent.mjb228.biometricdatacollection.R;
import com.unikent.mjb228.biometricdatacollection.face.FaceActivity;
import com.unikent.mjb228.biometricdatacollection.iris.factor.IrisFactorDarkActivity;

import static com.unikent.mjb228.biometricdatacollection.login.LoginActivity.PARTICIPANT_IDENTIFIER;
import static com.unikent.mjb228.biometricdatacollection.utilities.device.DeviceHelper.isFaceBiometricAvailable;

public class FaceFactorDarkActivity extends FaceActivity {

    private static final String FACTOR = "Factor - Dark";

    private static final String CHANGE_TO_FACE = "Change to Face";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_face_factor_dark);
        if (isFaceBiometricAvailable()) {
            ActivityResultLauncher<Intent> launcher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {

            });
            launcher.launch(new Intent(Settings.ACTION_SECURITY_SETTINGS));
            Toast.makeText(this, CHANGE_TO_FACE, Toast.LENGTH_SHORT).show();
            setupHandler();
            setupBufferedWriter(FACTOR);
        }
        super.onCreate(savedInstanceState);
    }

    public void startCamera(View view) {
        startCameraActivity(FACTOR);
    }

    public void startIrisFactorDark(View view) {
        Intent irisFactorDarkIntent = new Intent(this, IrisFactorDarkActivity.class);
        irisFactorDarkIntent.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        startActivity(irisFactorDarkIntent);
        finish();
    }

}
