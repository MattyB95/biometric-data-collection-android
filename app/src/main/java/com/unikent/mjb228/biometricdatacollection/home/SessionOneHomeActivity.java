package com.unikent.mjb228.biometricdatacollection.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.unikent.mjb228.biometricdatacollection.R;
import com.unikent.mjb228.biometricdatacollection.face.factor.FaceFactorDarkActivity;
import com.unikent.mjb228.biometricdatacollection.face.sitting.FaceSittingActivity;
import com.unikent.mjb228.biometricdatacollection.fingerprint.sitting.FingerprintSittingActivity;
import com.unikent.mjb228.biometricdatacollection.iris.sitting.IrisSittingActivity;
import com.unikent.mjb228.biometricdatacollection.voice.sitting.VoiceSittingActivity;

import org.jetbrains.annotations.NotNull;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static com.unikent.mjb228.biometricdatacollection.login.LoginActivity.PARTICIPANT_IDENTIFIER;
import static com.unikent.mjb228.biometricdatacollection.utilities.data.DataStorageHelper.getParticipantStorageDir;

public class SessionOneHomeActivity extends AppCompatActivity {

    private static final int REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION = 100;
    private static final int ZERO = 0;

    private String participantIdentifier;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session_one_home);
        createParticipantDirectory();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NotNull String[] permissions, @NotNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean permissionToWriteExternalStorageAccepted = false;
        if (requestCode == REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION) {
            permissionToWriteExternalStorageAccepted = grantResults[ZERO] == PERMISSION_GRANTED;
        }
        if (!permissionToWriteExternalStorageAccepted) {
            finish();
        }
    }

    private void createParticipantDirectory() {
        participantIdentifier = getIntent().getStringExtra(PARTICIPANT_IDENTIFIER);
        getParticipantStorageDir(this, participantIdentifier);
    }

    public void startFingerprintSitting(View view) {
        Intent fingerprintSittingIntent = new Intent(this, FingerprintSittingActivity.class);
        fingerprintSittingIntent.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        startActivity(fingerprintSittingIntent);
    }

    public void startFaceSitting(View view) {
        Intent faceSittingIntent = new Intent(this, FaceSittingActivity.class);
        faceSittingIntent.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        startActivity(faceSittingIntent);
    }

    public void startVoiceSitting(View view) {
        Intent voiceSittingIntent = new Intent(this, VoiceSittingActivity.class);
        voiceSittingIntent.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        startActivity(voiceSittingIntent);
    }

    public void startIrisSitting(View view) {
        Intent irisSittingIntent = new Intent(this, IrisSittingActivity.class);
        irisSittingIntent.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        startActivity(irisSittingIntent);
    }

    public void startFaceFactorDark(View view) {
        Intent faceFactorDarkIntent = new Intent(this, FaceFactorDarkActivity.class);
        faceFactorDarkIntent.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        startActivity(faceFactorDarkIntent);
    }

}
