package com.unikent.mjb228.biometricdatacollection.tour.modality;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;

import com.unikent.mjb228.biometricdatacollection.R;
import com.unikent.mjb228.biometricdatacollection.iris.IrisActivity;

import static com.unikent.mjb228.biometricdatacollection.login.LoginActivity.PARTICIPANT_IDENTIFIER;
import static com.unikent.mjb228.biometricdatacollection.tour.TourStopActivity.getNextStopIntent;
import static com.unikent.mjb228.biometricdatacollection.tour.stop.StopOneActivity.TOUR_NUMBER;
import static com.unikent.mjb228.biometricdatacollection.utilities.device.DeviceHelper.isIrisBiometricAvailable;

public class IrisTourActivity extends IrisActivity {

    private static final String TOUR = "Tour";

    private static final String CHANGE_TO_IRIS = "Change to Iris";

    private String scenario;
    private String tourNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        tourNumber = getIntent().getStringExtra(TOUR_NUMBER);
        scenario = TOUR + " - " + tourNumber;
        setContentView(R.layout.activity_iris_tour);
        if (isIrisBiometricAvailable()) {
            ActivityResultLauncher<Intent> launcher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
            });
            launcher.launch(new Intent(Settings.ACTION_SECURITY_SETTINGS));
            Toast.makeText(this, CHANGE_TO_IRIS, Toast.LENGTH_SHORT).show();
            setupHandler();
            setupBufferedWriter(scenario);
        }
        super.onCreate(savedInstanceState);
    }

    public void startCamera(View view) {
        startCameraActivity(scenario);
    }

    public void startNextStop(View view) {
        Intent nextStopIntent = getNextStopIntent(this, tourNumber);
        nextStopIntent.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        startActivity(nextStopIntent);
        finish();
    }

}
