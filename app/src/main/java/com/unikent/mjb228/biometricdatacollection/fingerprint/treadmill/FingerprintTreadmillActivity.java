package com.unikent.mjb228.biometricdatacollection.fingerprint.treadmill;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.unikent.mjb228.biometricdatacollection.R;
import com.unikent.mjb228.biometricdatacollection.fingerprint.FingerprintActivity;
import com.unikent.mjb228.biometricdatacollection.fingerprint.corridor.FingerprintCorridorActivity;

import static com.unikent.mjb228.biometricdatacollection.login.LoginActivity.PARTICIPANT_IDENTIFIER;

public class FingerprintTreadmillActivity extends FingerprintActivity {

    private static final String TREADMILL = "Treadmill";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_fingerprint_treadmill);
        setupHandler();
        setupBufferedWriter(TREADMILL);
        super.onCreate(savedInstanceState);
    }

    public void startBiometricAuthentication(View view) {
        useBiometricPrompt(TREADMILL);
    }

    public void startFingerprintCorridor(View view) {
        Intent fingerprintCorridorIntent = new Intent(this, FingerprintCorridorActivity.class);
        fingerprintCorridorIntent.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        startActivity(fingerprintCorridorIntent);
        finish();
    }

}
