package com.unikent.mjb228.biometricdatacollection.fingerprint;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.unikent.mjb228.biometricdatacollection.R;
import com.unikent.mjb228.biometricdatacollection.utilities.biometric.BiometricPromptHelper;
import com.unikent.mjb228.biometricdatacollection.utilities.sensors.SensorService;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static com.unikent.mjb228.biometricdatacollection.login.LoginActivity.PARTICIPANT_IDENTIFIER;
import static com.unikent.mjb228.biometricdatacollection.utilities.data.DataStorageHelper.getParticipantScenarioAttemptDir;
import static com.unikent.mjb228.biometricdatacollection.utilities.sensors.SensorService.ATTEMPT;
import static com.unikent.mjb228.biometricdatacollection.utilities.sensors.SensorService.MODALITY;
import static com.unikent.mjb228.biometricdatacollection.utilities.sensors.SensorService.SCENARIO;

public class FingerprintActivity extends AppCompatActivity {

    private static final String FINGERPRINT = "Fingerprint";

    private static final int ONE = 1;
    private static final int MINIMUM_ATTEMPTS = 5;
    private static final String FINGERPRINT_ACTIVITY_CSV = "/fingerprintActivity.csv";
    private static final String COMMA = ",";
    private static final String NEWLINE = "\n";

    private BiometricPromptHelper biometricPromptHelper;
    private BufferedWriter bufferedWriter;
    private Handler handler;
    private Intent sensorService;
    private long startNanoTime;

    protected String participantIdentifier;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        biometricPromptHelper = new BiometricPromptHelper(FINGERPRINT, this, handler);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (bufferedWriter != null) {
            try {
                bufferedWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
    }

    @NotNull
    private String getCurrentAttempt() {
        TextView counterView = findViewById(R.id.text_view_counter);
        String counterString = counterView.getText().toString();
        int counter = Integer.parseInt(counterString);
        int attempt = counter + ONE;
        return String.valueOf(attempt);
    }

    private void startSensorService(String scenario) {
        sensorService = new Intent(this, SensorService.class);
        sensorService.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        sensorService.putExtra(MODALITY, FINGERPRINT);
        sensorService.putExtra(SCENARIO, scenario);
        sensorService.putExtra(ATTEMPT, getCurrentAttempt());
        startService(sensorService);
    }

    private void updateCounter() {
        TextView counterView = findViewById(R.id.text_view_counter);
        String currentAttempt = getCurrentAttempt();
        counterView.setText(currentAttempt);
        if (Integer.parseInt(currentAttempt) >= MINIMUM_ATTEMPTS) {
            biometricPromptHelper.biometricPromptCancel();
            Button nextButton = findViewById(R.id.button_next);
            nextButton.setVisibility(View.VISIBLE);
        }
    }

    protected void setupHandler() {
        handler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(@NotNull Message message) {
                String timeElapsed = Long.toString(System.nanoTime() - startNanoTime);
                String currentTimeMillis = Long.toString(System.currentTimeMillis());
                try {
                    bufferedWriter.append(message.obj.toString())
                            .append(COMMA)
                            .append(timeElapsed)
                            .append(COMMA)
                            .append(currentTimeMillis)
                            .append(NEWLINE);
                    if (biometricPromptHelper.isAuthenticationComplete()) {
                        stopService(sensorService);
                        bufferedWriter.close();
                        updateCounter();
                    } else {
                        startNanoTime = System.nanoTime();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    protected void setupBufferedWriter(String scenario) {
        participantIdentifier = getIntent().getStringExtra(PARTICIPANT_IDENTIFIER);
        File fingerStorageDir = getParticipantScenarioAttemptDir(this, participantIdentifier, FINGERPRINT, scenario, getCurrentAttempt());
        String filePath = fingerStorageDir.getAbsolutePath() + FINGERPRINT_ACTIVITY_CSV;
        File file = new File(filePath);
        try {
            bufferedWriter = new BufferedWriter(new FileWriter(file));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void useBiometricPrompt(String scenario) {
        setupBufferedWriter(scenario);
        startSensorService(scenario);
        startNanoTime = System.nanoTime();
        biometricPromptHelper.biometricPromptAuthentication();
    }

}
