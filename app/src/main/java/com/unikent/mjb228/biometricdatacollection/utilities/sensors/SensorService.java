package com.unikent.mjb228.biometricdatacollection.utilities.sensors;

import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.os.IBinder;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static com.unikent.mjb228.biometricdatacollection.login.LoginActivity.PARTICIPANT_IDENTIFIER;
import static com.unikent.mjb228.biometricdatacollection.utilities.data.DataStorageHelper.getParticipantScenarioAttemptDir;

public class SensorService extends Service implements SensorEventListener {

    public static final String MODALITY = "MODALITY";
    public static final String SCENARIO = "SCENARIO";
    public static final String ATTEMPT = "ATTEMPT";

    private static final String SENSOR_DATA_CSV = "/sensorData.csv";
    private static final String COMMA = ",";
    private static final String NEWLINE = "\n";

    private SensorHandler sensorHandler;
    private BufferedWriter bufferedWriter;

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(@NotNull Intent intent, int flags, int startId) {
        String participantIdentifier = intent.getStringExtra(PARTICIPANT_IDENTIFIER);
        String activity = intent.getStringExtra(MODALITY);
        String scenario = intent.getStringExtra(SCENARIO);
        String attempt = intent.getStringExtra(ATTEMPT);
        setupBufferedWriter(participantIdentifier, activity, scenario, attempt);
        startReadingSensors();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        sensorHandler.unregisterListener(this);
    }

    private void setupBufferedWriter(String participantIdentifier, String activity, String scenario, String attempt) {
        File participantStorageDir = getParticipantScenarioAttemptDir(getApplicationContext(), participantIdentifier, activity, scenario, attempt);
        String filePath = participantStorageDir.getAbsolutePath() + SENSOR_DATA_CSV;
        try {
            bufferedWriter = new BufferedWriter(new FileWriter(filePath));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void startReadingSensors() {
        sensorHandler = new SensorHandler(getApplicationContext());
        activateSensors();
        sensorHandler.registerListener(this);
    }

    @SuppressWarnings("deprecation")
    private void activateSensors() {
        // Motion Sensors
        sensorHandler.activateDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorHandler.activateDefaultSensor(Sensor.TYPE_GRAVITY);
        sensorHandler.activateDefaultSensor(Sensor.TYPE_GYROSCOPE);
        sensorHandler.activateDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        sensorHandler.activateDefaultSensor(Sensor.TYPE_STEP_COUNTER);

        // Position Sensors
        sensorHandler.activateDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        sensorHandler.activateDefaultSensor(Sensor.TYPE_ORIENTATION);
        sensorHandler.activateDefaultSensor(Sensor.TYPE_PROXIMITY);

        // Environment Sensors
        sensorHandler.activateDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
        sensorHandler.activateDefaultSensor(Sensor.TYPE_LIGHT);
        sensorHandler.activateDefaultSensor(Sensor.TYPE_PRESSURE);
        sensorHandler.activateDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY);
        sensorHandler.activateDefaultSensor(Sensor.TYPE_TEMPERATURE);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onSensorChanged(@NotNull SensorEvent event) {
        String sensorType = event.sensor.getStringType();
        long currentTimeMillis = System.currentTimeMillis();
        SensorData sensorData = new SensorData(sensorType, event.values, currentTimeMillis);
        switch (event.sensor.getType()) {
            // Motion Sensors
            case Sensor.TYPE_ACCELEROMETER:
            case Sensor.TYPE_ACCELEROMETER_UNCALIBRATED:
            case Sensor.TYPE_GRAVITY:
            case Sensor.TYPE_GYROSCOPE:
            case Sensor.TYPE_GYROSCOPE_UNCALIBRATED:
            case Sensor.TYPE_LINEAR_ACCELERATION:
            case Sensor.TYPE_ROTATION_VECTOR:

                // Position Sensors
            case Sensor.TYPE_GAME_ROTATION_VECTOR:
            case Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR:
            case Sensor.TYPE_MAGNETIC_FIELD:
            case Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED:
            case Sensor.TYPE_ORIENTATION:
                writeCoordinateSensorData(sensorData);
                break;
            case Sensor.TYPE_STEP_COUNTER:
            case Sensor.TYPE_PROXIMITY:

                // Environment Sensors
            case Sensor.TYPE_AMBIENT_TEMPERATURE:
            case Sensor.TYPE_LIGHT:
            case Sensor.TYPE_PRESSURE:
            case Sensor.TYPE_RELATIVE_HUMIDITY:
            case Sensor.TYPE_TEMPERATURE:
                writeSensorData(sensorData);
                break;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    private void writeSensorType(String sensorType) {
        try {
            bufferedWriter.append(sensorType);
            bufferedWriter.append(COMMA);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeSensorData(@NotNull SensorData sensorData) {
        float sensorValue = sensorData.getSensorValue();
        String sensorValueString = Float.toString(sensorValue);
        writeSensorType(sensorData.getSensorType());
        appendSensorData(sensorValueString);
        writeTimestamp(sensorData.getTimestamp());
    }

    private void appendSensorData(String sensorValueString) {
        try {
            bufferedWriter.append(sensorValueString)
                    .append(COMMA);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeCoordinateSensorData(@NotNull SensorData sensorData) {
        float xAxisValue = sensorData.getXAxisValue();
        float yAxisValue = sensorData.getYAxisValue();
        float zAxisValue = sensorData.getZAxisValue();
        String xAxisValueString = Float.toString(xAxisValue);
        String yAxisValueString = Float.toString(yAxisValue);
        String zAxisValueString = Float.toString(zAxisValue);
        writeSensorType(sensorData.getSensorType());
        appendCoordinateSensorData(xAxisValueString, yAxisValueString, zAxisValueString);
        writeTimestamp(sensorData.getTimestamp());
    }

    private void appendCoordinateSensorData(String xAxisValueString, String yAxisValueString, String zAxisValueString) {
        try {
            bufferedWriter.append(xAxisValueString)
                    .append(COMMA)
                    .append(yAxisValueString)
                    .append(COMMA)
                    .append(zAxisValueString)
                    .append(COMMA);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeTimestamp(long timestampValue) {
        String timestampValueString = Long.toString(timestampValue);
        try {
            bufferedWriter.append(timestampValueString)
                    .append(NEWLINE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
