package com.unikent.mjb228.biometricdatacollection.fingerprint.factor;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;

import com.unikent.mjb228.biometricdatacollection.R;
import com.unikent.mjb228.biometricdatacollection.fingerprint.FingerprintActivity;
import com.unikent.mjb228.biometricdatacollection.home.SessionOneHomeActivity;

import static com.unikent.mjb228.biometricdatacollection.login.LoginActivity.PARTICIPANT_IDENTIFIER;
import static com.unikent.mjb228.biometricdatacollection.utilities.device.DeviceHelper.isFingerprintBiometricAvailable;

public class FingerprintFactorWetActivity extends FingerprintActivity {

    private static final String FACTOR = "Factor - Wet";

    private static final String CHANGE_TO_FINGERPRINT = "Change to Fingerprint";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_fingerprint_factor_wet);
        if (isFingerprintBiometricAvailable()) {
            ActivityResultLauncher<Intent> launcher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
            });
            launcher.launch(new Intent(Settings.ACTION_SECURITY_SETTINGS));
            Toast.makeText(this, CHANGE_TO_FINGERPRINT, Toast.LENGTH_SHORT).show();
        }
        setupHandler();
        setupBufferedWriter(FACTOR);
        super.onCreate(savedInstanceState);
    }

    public void startBiometricAuthentication(View view) {
        useBiometricPrompt(FACTOR);
    }

    public void startSessionOneHome(View view) {
        Intent sessionOneHomeIntent = new Intent(this, SessionOneHomeActivity.class);
        sessionOneHomeIntent.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        startActivity(sessionOneHomeIntent);
        finish();
    }

}
