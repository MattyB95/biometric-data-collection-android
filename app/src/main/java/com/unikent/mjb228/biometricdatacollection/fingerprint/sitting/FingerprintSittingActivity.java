package com.unikent.mjb228.biometricdatacollection.fingerprint.sitting;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;

import com.unikent.mjb228.biometricdatacollection.R;
import com.unikent.mjb228.biometricdatacollection.fingerprint.FingerprintActivity;
import com.unikent.mjb228.biometricdatacollection.fingerprint.standing.FingerprintStandingActivity;

import static com.unikent.mjb228.biometricdatacollection.login.LoginActivity.PARTICIPANT_IDENTIFIER;
import static com.unikent.mjb228.biometricdatacollection.utilities.device.DeviceHelper.isFingerprintBiometricAvailable;

public class FingerprintSittingActivity extends FingerprintActivity {

    private static final String SITTING = "Sitting";

    private static final String CHANGE_TO_FINGERPRINT = "Change to Fingerprint";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_fingerprint_sitting);
        if (isFingerprintBiometricAvailable()) {
            ActivityResultLauncher<Intent> launcher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
            });
            launcher.launch(new Intent(Settings.ACTION_SECURITY_SETTINGS));
            Toast.makeText(this, CHANGE_TO_FINGERPRINT, Toast.LENGTH_SHORT).show();
        }
        setupHandler();
        setupBufferedWriter(SITTING);
        super.onCreate(savedInstanceState);
    }

    public void startBiometricAuthentication(View view) {
        useBiometricPrompt(SITTING);
    }

    public void startFingerprintStanding(View view) {
        Intent fingerprintStandingIntent = new Intent(this, FingerprintStandingActivity.class);
        fingerprintStandingIntent.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        startActivity(fingerprintStandingIntent);
        finish();
    }

}
