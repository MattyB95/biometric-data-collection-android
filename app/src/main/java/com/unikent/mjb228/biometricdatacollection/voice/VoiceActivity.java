package com.unikent.mjb228.biometricdatacollection.voice;

import android.content.Intent;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.unikent.mjb228.biometricdatacollection.R;
import com.unikent.mjb228.biometricdatacollection.utilities.sensors.SensorService;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static android.graphics.Color.BLACK;
import static android.graphics.Color.BLUE;
import static android.graphics.Color.GREEN;
import static android.graphics.Color.RED;
import static com.unikent.mjb228.biometricdatacollection.login.LoginActivity.PARTICIPANT_IDENTIFIER;
import static com.unikent.mjb228.biometricdatacollection.utilities.data.DataStorageHelper.FORWARD_SLASH;
import static com.unikent.mjb228.biometricdatacollection.utilities.data.DataStorageHelper.getParticipantScenarioAttemptDir;
import static com.unikent.mjb228.biometricdatacollection.utilities.data.FilenameHelper.getTimestampedAudioFileName;
import static com.unikent.mjb228.biometricdatacollection.utilities.sensors.SensorService.ATTEMPT;
import static com.unikent.mjb228.biometricdatacollection.utilities.sensors.SensorService.MODALITY;
import static com.unikent.mjb228.biometricdatacollection.utilities.sensors.SensorService.SCENARIO;

public class VoiceActivity extends AppCompatActivity {

    private static final String VOICE = "Voice";

    private static final int ZERO = 0;
    private static final int ONE = 1;
    private static final int MINIMUM_ATTEMPTS = 5;
    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;

    private MediaRecorder mediaRecorder;
    private String voiceFilePath;
    private boolean startRecording;

    protected String participantIdentifier;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Button recordButton = findViewById(R.id.button_record);
        recordButton.setTextColor(BLUE);
        recordButton.setBackgroundColor(GREEN);
        checkPermissions();
        startRecording = true;
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mediaRecorder != null) {
            mediaRecorder.release();
            mediaRecorder = null;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NotNull String[] permissions, @NotNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean permissionToRecordAccepted = false;
        if (requestCode == REQUEST_RECORD_AUDIO_PERMISSION) {
            permissionToRecordAccepted = grantResults[ZERO] == PERMISSION_GRANTED;
        }
        if (!permissionToRecordAccepted) finish();
    }

    private void checkPermissions() {
        String[] permissions = {RECORD_AUDIO};
        ActivityCompat.requestPermissions(this, permissions, REQUEST_RECORD_AUDIO_PERMISSION);
    }

    private void startRecording(String scenario) {
        displayVoicePhrase();
        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setOutputFile(voiceFilePath);
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        try {
            mediaRecorder.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
        startSensorService(scenario);
        mediaRecorder.start();
    }

    private void displayVoicePhrase() {
        TextView counterView = findViewById(R.id.text_view_counter);
        if (counterView.getVisibility() == View.GONE) {
            counterView.setVisibility(View.VISIBLE);
        } else {
            counterView.setVisibility(View.GONE);
        }
        TextView voicePhraseView = findViewById(R.id.text_view_voice_phrase);
        if (voicePhraseView.getVisibility() == View.GONE) {
            voicePhraseView.setVisibility(View.VISIBLE);
        } else {
            voicePhraseView.setVisibility(View.GONE);
        }
    }

    private void startSensorService(String scenario) {
        Intent service = new Intent(this, SensorService.class);
        service.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        service.putExtra(MODALITY, VOICE);
        service.putExtra(SCENARIO, scenario);
        service.putExtra(ATTEMPT, getCurrentAttempt());
        startService(service);
    }

    @NotNull
    private String getCurrentAttempt() {
        TextView counterView = findViewById(R.id.text_view_counter);
        String counterString = counterView.getText().toString();
        int counter = Integer.parseInt(counterString);
        int attempt = counter + ONE;
        return String.valueOf(attempt);
    }

    private void onRecord(boolean start, String scenario) {
        if (start) {
            startRecording(scenario);
        } else {
            stopRecording(scenario);
        }
    }

    private void stopRecording(String scenario) {
        mediaRecorder.stop();
        mediaRecorder.release();
        mediaRecorder = null;
        stopService(new Intent(this, SensorService.class));
        displayVoicePhrase();
        updateCounter();
        createVoiceFile(scenario);
    }

    private void updateCounter() {
        TextView counterView = findViewById(R.id.text_view_counter);
        String currentAttempt = getCurrentAttempt();
        counterView.setText(currentAttempt);
        if (Integer.parseInt(currentAttempt) >= MINIMUM_ATTEMPTS) {
            Button nextButton = findViewById(R.id.button_next);
            nextButton.setVisibility(View.VISIBLE);
        }
    }

    protected void createVoiceFile(String scenario) {
        participantIdentifier = getIntent().getStringExtra(PARTICIPANT_IDENTIFIER);
        File voiceStorageDir = getParticipantScenarioAttemptDir(this, participantIdentifier, VOICE, scenario, getCurrentAttempt());
        String voiceFilename = participantIdentifier + getTimestampedAudioFileName(VOICE);
        voiceFilePath = voiceStorageDir.getAbsolutePath() + FORWARD_SLASH + voiceFilename;
    }

    protected void recordAction(Button recordButton, String scenario) {
        onRecord(startRecording, scenario);
        if (startRecording) {
            recordButton.setText(getString(R.string.stop_recording));
            recordButton.setTextColor(BLACK);
            recordButton.setBackgroundColor(RED);
        } else {
            recordButton.setText(getString(R.string.start_recording));
            recordButton.setTextColor(BLUE);
            recordButton.setBackgroundColor(GREEN);
        }
        startRecording = !startRecording;
    }

}
