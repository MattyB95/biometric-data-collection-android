package com.unikent.mjb228.biometricdatacollection.login;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.unikent.mjb228.biometricdatacollection.R;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

public class LoginActivity extends AppCompatActivity {

    public static final String PARTICIPANT_IDENTIFIER = "PARTICIPANT_IDENTIFIER";
    private static final String DEBUG = "Debug";

    private EditText participantIdentifierView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        participantIdentifierView = findViewById(R.id.participant_identifier);
        participantIdentifierView.setOnEditorActionListener(getOnEditorActionListener());
    }

    @Contract(value = " -> new", pure = true)
    @NotNull
    private TextView.OnEditorActionListener getOnEditorActionListener() {
        return (v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_NULL) {
                attemptLogin();
                return true;
            }
            return false;
        };
    }

    public void begin(View view) {
        attemptLogin();
    }

    private void attemptLogin() {
        String participantIdentifier = participantIdentifierView.getText().toString();
        if (TextUtils.isEmpty(participantIdentifier)) {
            participantIdentifier = DEBUG;
        }
        startHomeActivity(participantIdentifier);
    }

    private void startHomeActivity(String participantIdentifier) {
        Intent sessionPickerIntent = new Intent(this, SessionPicker.class);
        sessionPickerIntent.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        startActivity(sessionPickerIntent);
        finish();
    }

}
