package com.unikent.mjb228.biometricdatacollection.utilities.sensors;

import org.jetbrains.annotations.NotNull;

class SensorData {

    private static final int ZERO = 0;
    private static final int ONE = 1;
    private static final int TWO = 2;
    private static final int THREE = 3;

    private final String sensorType;
    private final long timestamp;
    private final float sensorValue;
    private float xAxisValue;
    private float yAxisValue;
    private float zAxisValue;

    SensorData(String sensorType, @NotNull float[] sensorValues, long timestamp) {
        this.sensorType = sensorType;
        sensorValue = sensorValues[ZERO];
        if (sensorValues.length == THREE) {
            xAxisValue = sensorValues[ZERO];
            yAxisValue = sensorValues[ONE];
            zAxisValue = sensorValues[TWO];
        }
        this.timestamp = timestamp;
    }

    String getSensorType() {
        return sensorType;
    }

    float getXAxisValue() {
        return xAxisValue;
    }

    float getYAxisValue() {
        return yAxisValue;
    }

    float getZAxisValue() {
        return zAxisValue;
    }

    float getSensorValue() {
        return sensorValue;
    }

    long getTimestamp() {
        return timestamp;
    }

}
