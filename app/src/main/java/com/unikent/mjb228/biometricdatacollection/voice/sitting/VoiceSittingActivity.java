package com.unikent.mjb228.biometricdatacollection.voice.sitting;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.unikent.mjb228.biometricdatacollection.R;
import com.unikent.mjb228.biometricdatacollection.voice.VoiceActivity;
import com.unikent.mjb228.biometricdatacollection.voice.standing.VoiceStandingActivity;

import static com.unikent.mjb228.biometricdatacollection.login.LoginActivity.PARTICIPANT_IDENTIFIER;

public class VoiceSittingActivity extends VoiceActivity {

    private static final String SITTING = "Sitting";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_voice_sitting);
        createVoiceFile(SITTING);
        super.onCreate(savedInstanceState);
    }

    public void record(View view) {
        recordAction((Button) view, SITTING);
    }

    public void startVoiceStanding(View view) {
        Intent voiceStandingIntent = new Intent(this, VoiceStandingActivity.class);
        voiceStandingIntent.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        startActivity(voiceStandingIntent);
        finish();
    }

}
