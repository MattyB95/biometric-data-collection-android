package com.unikent.mjb228.biometricdatacollection.utilities.iritech;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class IriShieldUIHelper {

    private static final String NOT_QUALIFIED = "Eye Image Is Not Qualified";

    private final ImageView iriShieldView;
    private final Button captureButton;
    private final Button nextButton;

    public IriShieldUIHelper(ImageView iriShieldView, Button captureButton, Button nextButton) {
        this.iriShieldView = iriShieldView;
        this.captureButton = captureButton;
        this.nextButton = nextButton;
    }

    void displayImage(final Bitmap bitmap) {
        new Handler(Looper.getMainLooper()).post(() -> iriShieldView.setImageBitmap(bitmap));
    }

    void enableCaptureButton() {
        new Handler(Looper.getMainLooper()).post(() -> captureButton.setEnabled(true));
    }

    void enableNextButton() {
        new Handler(Looper.getMainLooper()).post(() -> nextButton.setEnabled(true));
    }

    void showToast() {
        new Handler(Looper.getMainLooper()).post(() -> Toast.makeText(iriShieldView.getContext(), NOT_QUALIFIED, Toast.LENGTH_SHORT).show());
    }

}
