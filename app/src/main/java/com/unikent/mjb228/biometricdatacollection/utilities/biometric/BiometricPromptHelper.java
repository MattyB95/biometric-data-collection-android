package com.unikent.mjb228.biometricdatacollection.utilities.biometric;

import android.os.Handler;
import android.os.Message;

import androidx.biometric.BiometricPrompt;
import androidx.fragment.app.FragmentActivity;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class BiometricPromptHelper {

    private static final String AUTHENTICATE_WITH_YOUR = "Attempt to authenticate with your ";
    private static final String SUBTITLE = "Authentication";
    private static final String NEGATIVE_BUTTON = "Cancel";
    private static final String NEGATIVE_BUTTON_PRESSED = "Negative Button";
    private static final String SUCCEEDED = "Authentication Succeeded";
    private static final String FAILED = "Authentication Failed";
    private static final int COMMAND = 100;

    private final String modality;
    private final String description;
    private final BiometricPrompt biometricPrompt;
    private final BiometricPrompt.PromptInfo biometricPromptInfo;

    private boolean isAuthenticationOver;

    public BiometricPromptHelper(String modality, FragmentActivity fragmentActivity, Handler handler) {
        this.modality = modality;
        this.description = AUTHENTICATE_WITH_YOUR + modality;
        biometricPrompt = getBiometricPrompt(fragmentActivity, handler);
        biometricPromptInfo = getBiometricPromptInfo();
    }

    @NotNull
    private BiometricPrompt getBiometricPrompt(FragmentActivity fragmentActivity, Handler handler) {
        Executor executor = Executors.newSingleThreadExecutor();
        return new BiometricPrompt(fragmentActivity, executor, getBiometricCallback(handler));
    }

    @NotNull
    @Contract(value = "_ -> new", pure = true)
    private BiometricPrompt.AuthenticationCallback getBiometricCallback(final Handler handler) {
        return new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode, @NotNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
                isAuthenticationOver = true;
                Message message;
                if (errorCode == BiometricPrompt.ERROR_NEGATIVE_BUTTON) {
                    message = handler.obtainMessage(COMMAND, NEGATIVE_BUTTON_PRESSED);
                } else {
                    message = handler.obtainMessage(COMMAND, errString);
                }
                message.sendToTarget();
            }

            @Override
            public void onAuthenticationSucceeded(@NotNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
                isAuthenticationOver = true;
                Message message = handler.obtainMessage(COMMAND, SUCCEEDED);
                message.sendToTarget();
            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
                isAuthenticationOver = false;
                Message message = handler.obtainMessage(COMMAND, FAILED);
                message.sendToTarget();
            }
        };
    }

    @NotNull
    private BiometricPrompt.PromptInfo getBiometricPromptInfo() {
        return new BiometricPrompt.PromptInfo.Builder()
                .setTitle(modality)
                .setSubtitle(SUBTITLE)
                .setDescription(description)
                .setNegativeButtonText(NEGATIVE_BUTTON)
                .build();
    }

    public void biometricPromptAuthentication() {
        isAuthenticationOver = false;
        biometricPrompt.authenticate(biometricPromptInfo);
    }

    public void biometricPromptCancel() {
        biometricPrompt.cancelAuthentication();
    }

    public boolean isAuthenticationComplete() {
        return isAuthenticationOver;
    }

}
