package com.unikent.mjb228.biometricdatacollection.tour.modality;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.unikent.mjb228.biometricdatacollection.R;
import com.unikent.mjb228.biometricdatacollection.voice.VoiceActivity;

import static com.unikent.mjb228.biometricdatacollection.login.LoginActivity.PARTICIPANT_IDENTIFIER;
import static com.unikent.mjb228.biometricdatacollection.tour.stop.StopOneActivity.TOUR_NUMBER;

public class VoiceTourActivity extends VoiceActivity {

    private static final String TOUR = "Tour";

    private String tourNumber;
    private String scenario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        tourNumber = getIntent().getStringExtra(TOUR_NUMBER);
        scenario = TOUR + " - " + tourNumber;
        setContentView(R.layout.activity_voice_tour);
        createVoiceFile(scenario);
        super.onCreate(savedInstanceState);
    }

    public void record(View view) {
        recordAction((Button) view, scenario);
    }

    public void startIrisTour(View view) {
        Intent irisTourIntent = new Intent(this, IrisTourActivity.class);
        irisTourIntent.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        irisTourIntent.putExtra(TOUR_NUMBER, tourNumber);
        startActivity(irisTourIntent);
        finish();
    }

}
