package com.unikent.mjb228.biometricdatacollection.iris.factor;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;

import com.unikent.mjb228.biometricdatacollection.R;
import com.unikent.mjb228.biometricdatacollection.iris.IrisActivity;
import com.unikent.mjb228.biometricdatacollection.voice.factor.VoiceFactorQuietActivity;

import static com.unikent.mjb228.biometricdatacollection.login.LoginActivity.PARTICIPANT_IDENTIFIER;
import static com.unikent.mjb228.biometricdatacollection.utilities.device.DeviceHelper.isIrisBiometricAvailable;

public class IrisFactorDarkActivity extends IrisActivity {

    private static final String FACTOR = "Factor - Dark";

    private static final String CHANGE_TO_IRIS = "Change to Iris";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_iris_factor_dark);
        if (isIrisBiometricAvailable()) {
            ActivityResultLauncher<Intent> launcher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
            });
            launcher.launch(new Intent(Settings.ACTION_SECURITY_SETTINGS));
            Toast.makeText(this, CHANGE_TO_IRIS, Toast.LENGTH_SHORT).show();
            setupHandler();
            setupBufferedWriter(FACTOR);
        }
        super.onCreate(savedInstanceState);
    }

    public void startCamera(View view) {
        startCameraActivity(FACTOR);
    }

    public void startFaceFactorLight(View view) {
        Intent voiceFactorQuietIntent = new Intent(this, VoiceFactorQuietActivity.class);
        voiceFactorQuietIntent.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        startActivity(voiceFactorQuietIntent);
        finish();
    }

}
