package com.unikent.mjb228.biometricdatacollection.iris.standing;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.unikent.mjb228.biometricdatacollection.R;
import com.unikent.mjb228.biometricdatacollection.iris.IrisActivity;
import com.unikent.mjb228.biometricdatacollection.iris.treadmill.IrisTreadmillActivity;

import static com.unikent.mjb228.biometricdatacollection.login.LoginActivity.PARTICIPANT_IDENTIFIER;
import static com.unikent.mjb228.biometricdatacollection.utilities.device.DeviceHelper.isIrisBiometricAvailable;

public class IrisStandingActivity extends IrisActivity {

    private static final String STANDING = "Standing";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_iris_standing);
        if (isIrisBiometricAvailable()) {
            setupHandler();
            setupBufferedWriter(STANDING);
        }
        super.onCreate(savedInstanceState);
    }

    public void startCamera(View view) {
        startCameraActivity(STANDING);
    }

    public void startIrisTreadmill(View view) {
        Intent irisTreadmillIntent = new Intent(this, IrisTreadmillActivity.class);
        irisTreadmillIntent.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        startActivity(irisTreadmillIntent);
        finish();
    }

}
