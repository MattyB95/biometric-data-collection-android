package com.unikent.mjb228.biometricdatacollection.fingerprint.standing;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.unikent.mjb228.biometricdatacollection.R;
import com.unikent.mjb228.biometricdatacollection.fingerprint.FingerprintActivity;
import com.unikent.mjb228.biometricdatacollection.fingerprint.treadmill.FingerprintTreadmillActivity;

import static com.unikent.mjb228.biometricdatacollection.login.LoginActivity.PARTICIPANT_IDENTIFIER;

public class FingerprintStandingActivity extends FingerprintActivity {

    private static final String STANDING = "Standing";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_fingerprint_standing);
        setupHandler();
        setupBufferedWriter(STANDING);
        super.onCreate(savedInstanceState);
    }

    public void startBiometricAuthentication(View view) {
        useBiometricPrompt(STANDING);
    }

    public void startFingerprintTreadmill(View view) {
        Intent fingerprintTreadmillIntent = new Intent(this, FingerprintTreadmillActivity.class);
        fingerprintTreadmillIntent.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        startActivity(fingerprintTreadmillIntent);
        finish();
    }

}
