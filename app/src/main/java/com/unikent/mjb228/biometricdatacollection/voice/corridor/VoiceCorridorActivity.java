package com.unikent.mjb228.biometricdatacollection.voice.corridor;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.unikent.mjb228.biometricdatacollection.R;
import com.unikent.mjb228.biometricdatacollection.home.SessionOneHomeActivity;
import com.unikent.mjb228.biometricdatacollection.voice.VoiceActivity;

import static com.unikent.mjb228.biometricdatacollection.login.LoginActivity.PARTICIPANT_IDENTIFIER;

public class VoiceCorridorActivity extends VoiceActivity {

    private static final String CORRIDOR = "Corridor";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_voice_corridor);
        createVoiceFile(CORRIDOR);
        super.onCreate(savedInstanceState);
    }

    public void record(View view) {
        recordAction((Button) view, CORRIDOR);
    }

    public void startSessionOneHome(View view) {
        Intent sessionOneHomeIntent = new Intent(this, SessionOneHomeActivity.class);
        sessionOneHomeIntent.putExtra(PARTICIPANT_IDENTIFIER, participantIdentifier);
        startActivity(sessionOneHomeIntent);
        finish();
    }

}
